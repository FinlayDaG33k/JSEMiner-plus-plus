#include "stdafx.h"
#include "JSEMiner.h"
#include "sha256.h"
#include <exception>
#include <fstream>
#include <curl/curl.h>
#include "JSEUtils.h"
#include <regex>
#include <thread>
#include <iomanip>
#include <random>

namespace JSEMiner {

	size_t WriteCallback(char *contents, size_t size, size_t nmemb, void *userp)
	{
		((std::string*)userp)->append((char*)contents, size * nmemb);
		return size * nmemb;
	}

	Miner::Miner(int numThreads, std::string apiKey) {
		std::ifstream trackfile("track.json");
		json j;
		trackfile >> j;
		Track = j;
		trackfile.close();
		this->numThreads = numThreads;
		this->apiKey = apiKey;
	}

	std::string Miner::JSEPostTrack(std::string url, std::string content) {
		CURL *curl = curl_easy_init();
		CURLcode res;
		std::string bodyContent = "";
		struct curl_slist *list = NULL;
		if (curl) {
			content = std::string("o=") + std::string(curl_easy_escape(curl, content.c_str(), content.length()));
			list = curl_slist_append(list, "Accept: */*");
			list = curl_slist_append(list, "Content-Type: application/x-www-form-urlencoded");
			list = curl_slist_append(list, (std::string("Origin:") + Track.url).c_str());
			list = curl_slist_append(list, (std::string("Referer:") + Track.url).c_str());
			list = curl_slist_append(list, "Cache-Control: no-cache");
			list = curl_slist_append(list, "Connection: keep-alive");
			list = curl_slist_append(list, "Accept-Encoding: gzip, deflate, br");
			list = curl_slist_append(list, "Accept-Language: nl-NL,nl;q=0.8,en-US;q=0.6,en;q=0.4");
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
			curl_easy_setopt(curl, CURLOPT_POST, 1);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, content.c_str());
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &bodyContent);
			curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 20000);
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 0);
			res = curl_easy_perform(curl);
		}
		curl_slist_free_all(list);
		curl_easy_cleanup(curl);
		return bodyContent;
	}

	std::string Miner::JSEGet(std::string url) {
		CURL *curl = curl_easy_init();
		CURLcode res;
		std::string bodyContent = "";
		struct curl_slist *list = NULL;
		if (curl) {
			list = curl_slist_append(list, "Accept: */*");
			list = curl_slist_append(list, "Content-Type: application/x-www-form-urlencoded");
			list = curl_slist_append(list, (std::string("Origin:") + Track.url).c_str());
			list = curl_slist_append(list, (std::string("Referer:") + Track.url).c_str());
			list = curl_slist_append(list, "Cache-Control: no-cache");
			list = curl_slist_append(list, "Connection: keep-alive");
			list = curl_slist_append(list, "Accept-Encoding: gzip, deflate, br");
			list = curl_slist_append(list, "Accept-Language: nl-NL,nl;q=0.8,en-US;q=0.6,en;q=0.4");
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &bodyContent);
			curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 20000);
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 0);
			res = curl_easy_perform(curl);
		}
		curl_slist_free_all(list);
		curl_easy_cleanup(curl);
		return bodyContent;
	}

	bool Miner::RequestNewBlock() {
		std::string response = JSEPostTrack(JSE_LOAD_SERVER + "/request/", "1");
		if (response == "")
			return false;
		try {
			auto j = json::parse(response);
			CurrentBlock = j;
			BlockJSON = response;
			blocksReceived++;
		}
		catch (std::exception& ex) {
			return false;
		}
		return true;
	}

	bool Miner::PostHit() {
		Track.hits++;
		std::ofstream trackout("track.json");
		json j = Track;
		trackout << j;
		trackout.close();

		std::string trackStr = j.dump();
		if (Track.hits > 3) {
			JSEHIT hit;
			hit.pubid = Track.pubid;
			hit.siteid = Track.siteid;
			hit.subid = Track.subid;
			j = hit;
			trackStr = j.dump();
		}
		std::string resp = JSEPostTrack(JSE_SERVER + "/save/", trackStr);
		return true;
	}

	bool Miner::ProcessHash(HASHRESULT *res, int difficulty) {
		if (res->hash.substr(0, difficulty) == std::string(difficulty, '0')) {
			if (res->hash == lastHash || CurrentBlock.blockID == lastID)
				return false;
			lastHash = res->hash;
			lastID = CurrentBlock.blockID;
			hashesFound++;
			JSEPOSTBLOCK block;
			block.blockID = CurrentBlock.blockID;
			block.hash = res->hash;
			block.nonce = res->nonce;
			block.pubid = Track.pubid;
			block.siteid = Track.siteid;
			block.subid = Track.subid;
			block.uniq = Track.uniq;
			json j = block;
			std::string resp = JSEPostTrack(JSE_SERVER + "/submit/", j.dump());
			GetAPIBalance();
			return true;
		}
		return false;
	}

	void Miner::GetAPIBalance() {
		try {
			std::string url = std::string("https://api.jsecoin.com/server/api/balance/") + apiKey + std::string("/0/");
			std::string body = JSEGet(url);
			json j = json::parse(body);
			api_balance = roundf(j["balance"].get<double>() * 1000) / 1000;
		}
		catch (std::exception& ex) {}
	}

	bool Miner::DoHash(std::string in, int nonce, HASHRESULT *res) {
		res->hash = sha256(in);
		res->nonce = nonce;
		return true;
	}

	std::string tail(const std::string& str, size_t length) {
		std::string s_tail;
		if (length < str.size()) {
			std::reverse_copy(str.rbegin(), str.rbegin() + length, std::back_inserter(s_tail));
		}
		return s_tail;
	}


	void Miner::ticker() {
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
		while (true) {
			Sleep(1000);
			HashesPerSecond = hashes;
			hashes = 0;
			PrintInfo();
		}
	}

	void Miner::PrintInfo() {
		COORD c{ 0,0 };
		SetConsoleCursorPosition(
			GetStdHandle(STD_OUTPUT_HANDLE),
			c
		);
		std::chrono::system_clock::time_point time = std::chrono::system_clock::now();
		std::chrono::duration<double> runTime = time - std::chrono::system_clock::from_time_t(startTime);
		auto t = tail(lastHash, 5);
		std::cout << "=============== JSEMiner++ by Mordecaii (spike2147) & FinlayDaG33k ===============" << "\n";
		std::cout << "Start time: " << std::put_time(std::localtime(&startTime), "%T") << "\t";
		std::cout << "Runtime: " << std::setfill('0') << std::setw(2) << std::chrono::duration_cast<std::chrono::hours>(runTime).count();
		std::cout << ":" << std::setfill('0') << std::setw(2) << std::chrono::duration_cast<std::chrono::minutes>(runTime).count() % 60;
		std::cout << ":" << std::setfill('0') << std::setw(2) << std::chrono::duration_cast<std::chrono::seconds>(runTime).count() % 60 << std::setw(0) << "\t";
		std::cout << "Hashrate: " << (HashesPerSecond == 0 ? "Idle" : std::to_string(HashesPerSecond) + "/s") << " (" << this->numThreads << " threads)\t\t\n";
		std::cout << "Hashes found: " << hashesFound << "\t\tLast hash: " << (t == "" ? "00000" : t) << "\tUID: " << Track.uniq << "\n";
		std::cout << "Current block: " << CurrentBlock.blockID << "\tBlocks received: " << blocksReceived;
		if (apiKey != "")
			std::cout << "\tBalance: " << api_balance << " JSE";
		std::cout << "\n" << "==================================================================================" << std::endl;
	}

	void Miner::Start() {
		std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
		startTime = std::chrono::system_clock::to_time_t(today);
		std::thread tickT(&Miner::ticker, this);
		PostHit();
		GetAPIBalance();
		while (true) {
			std::thread t[32]; //max 32
			hashFound = false;
			if (RequestNewBlock()) {
				for (int i = 0; i < numThreads; ++i) {
					t[i] = std::thread(&Miner::JSEMine, this);
				}
				for (auto& th : t)
					if (th.joinable())
						th.join();
			}
		}
	}

	long Miner::Rand() {
		std::mt19937 rng;
		// initialize the random number generator with time-dependent seed
		uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
		std::seed_seq ss{ uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32) };
		rng.seed(ss);
		// initialize a uniform distribution between 0 and 1
		std::uniform_real_distribution<double> unif(0, 1);
		double rnd = unif(rng);
		return floor(99999999 * rnd);
	}

	void Miner::JSEMine() {
		int e = CurrentBlock.difficulty;
		__int64 p = CurrentBlock.startTime + 2 * CurrentBlock.frequency + 3000;
		long o = Rand();
		int rounds = 0;
		for (; !(JSEUtils::GetTime() > p || hashFound); rounds++) {
			o++;
			if (rounds % 200000 == 0) { rounds = 0; o = Rand(); }
			HASHRESULT res;
			std::string targetTextWithNonce = std::regex_replace(BlockJSON, std::regex("\\*nonce\\*"), std::to_string(o));
			DoHash(targetTextWithNonce, o, &res);
			hashFound = ProcessHash(&res, e);
			hashes++;
		}
		hashes = 0;
	}

	/* JSON CONVERTION TYPES*/
	void to_json(json& j, const JSETRACK& p) {
		j = json{ 
			{ "pubid", p.pubid },
			{ "siteid", p.siteid },
			{ "subid", p.subid },
			{ "userip", p.userip },
			{ "geo", p.geo },
			{ "useragent", p.useragent },
			{ "device", p.device },
			{ "browser", p.browser },
			{ "os", p.os },
			{ "referrer", p.referrer },
			{ "url", p.url },
			{ "language", p.language },
			{ "uniq", p.uniq },
			{ "hits", p.hits }
		};
	}
	void from_json(const json& j, JSETRACK& p) {
		p.pubid = j.at("pubid").get<std::string>();
		p.siteid = j.at("siteid").get<std::string>();
		p.subid = j.at("subid").get<std::string>();
		p.userip = j.at("userip").get<std::string>();
		p.geo = j.at("geo").get<std::string>();
		p.useragent = j.at("useragent").get<std::string>();
		p.device = j.at("device").get<std::string>();
		p.browser = j.at("browser").get<std::string>();
		p.os = j.at("os").get<std::string>();
		p.referrer = j.at("referrer").get<std::string>();
		p.url = j.at("url").get<std::string>();
		p.language = j.at("language").get<std::string>();
		p.uniq = j.at("uniq").get<std::string>();
		p.hits = j.at("hits").get<int>();
	}

	void to_json(json& j, const JSEMINERBLOCK& p) {
		j = json{
			{"blockID", p.blockID},
			{"difficulty", p.difficulty},
			{"frequency", p.frequency},
			{"hash", p.hash},
			{"mainChain", p.mainChain},
			{"nonce", p.nonce},
			{"server", p.server},
			{"startTime", p.startTime},
			{"version", p.version},
			{"blockReference", p.blockReference},
			{"open", p.open}
		};
	}

	void from_json(const json& j, JSEMINERBLOCK& p) {
		p.blockID = j.at("blockID").get<int>();
		p.difficulty = j.at("difficulty").get<int>();
		p.frequency = j.at("frequency").get<int>();
		p.hash = j.at("hash").get<std::string>();
		p.mainChain = j.at("mainChain").get<int>();
		p.nonce = j.at("nonce").get<std::string>();
		p.server = j.at("server").get<std::string>();
		p.startTime = j.at("startTime").get<__int64>();
		p.version = j.at("version").get<std::string>();
		p.blockReference = j.at("blockReference").get<int>();
		p.open = j.at("open").get<bool>();
	}

	void to_json(json& j, const JSEHIT& p) {
		j = json{
			{ "pubid", p.pubid },
			{ "siteid", p.siteid },
			{"subid", p.subid },
			{"sendHit", p.sendHit}
		};
	}

	void from_json(const json& j, JSEHIT& p) { 
		p.pubid = j.at("pubid").get<std::string>();
		p.siteid = j.at("siteid").get<std::string>();
		p.subid = j.at("subid").get<std::string>();
		p.sendHit = j.at("sendHit").get<int>();
	}

	void to_json(json& j, const JSEPOSTBLOCK& p) {
		j = json{
			{ "pubid", p.pubid },
			{ "siteid", p.siteid },
			{ "subid", p.subid },
			{ "blockID", p.blockID },
			{ "hash", p.hash },
			{ "uniq", p.uniq },
			{ "nonce", std::to_string(p.nonce) },
		};
	}
}