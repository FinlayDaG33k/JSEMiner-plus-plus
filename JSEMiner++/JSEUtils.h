#pragma once
#include <chrono>
class JSEUtils {
public:
	static __int64 GetTime() { return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count(); }
};